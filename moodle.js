const moodle_host = "moodle.domain.com";
const moodle_upload = "/moodle/webservice/upload.php";
const moodle_rest = "/moodle/webservice/rest/server.php";

const implemented_modules = ["Files", "Folders"];

var http = require("http");
var request = require("request");
var querystring = require("querystring");
var Qs = require("qs");
var assert = require("assert");

/*
 * Internal: Handles actual communication with moodle. Issues authenticated POST
 * request and returns parsed JSON.
 */
function moodle_rest_call(token, api_function, query, callback) {
  var parameters = querystring.stringify({
    wstoken: token,
    wsfunction: api_function,
    moodlewsrestformat: 'json'
  });

  var post_options = {
    host: moodle_host,
    port: '80',
    method: 'POST',
    path: moodle_rest + "?" + parameters,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': query.length
    }
  };

  var req = http.request(post_options, function(res) {
    body = "";
    res.on("data", function(chunk) {
      body += chunk;
    });
    res.on("end", function() {
      var result;
      try {
        result = JSON.parse(body);
      } catch (err) {
        callback(err);
      }
      if (result) {
        if (result.exception) {
          callback(new Error(result.message));
        } else {
          callback(null, result);
        }
      }
    })
  });
  req.on("error", function(err) {
    callback(err);
  });
  req.write(query);
  req.end();
};

/*
 * Returns information about users that match the specified criterion.
 * Allowed keys: id, lastname, firstname, idnumber, username, email, auth
 */
exports.getUsersBy = function(token, key, value, callback) {
  moodle_rest_call(token, "core_user_get_users", Qs.stringify({
    criteria: [{
      key: key,
      value: value
    }]
  }), callback);
};

/*
 * Returns information about a uniquely specified users.
 * Allowed keys: id, idnumber, username, email. Fast than getUserBy().
 */
exports.getSingleUserBy = function(token, key, value, callback) {
  moodle_rest_call(token, "core_user_get_users_by_field", Qs.stringify({
      field: key,
      values: [value]
    }),
    function(err, user) {
      if (err) {
        callback(err);
      } else {
        callback(null, user[0]);
      }
    });
};

/*
 * Returns some generic information about the moodle (+ the user the token is
 * associated with).
 */
exports.getSiteInfo = function(token, callback) {
  moodle_rest_call(token, "core_webservice_get_site_info", "", callback);
};

/*
 * Returns information about the courses whose ID is given in the courseids
 * array. If courseids is null or empty all courses are returned.
 */
exports.getCoursesByID = function(token, courseids, callback) {
  var query;
  if (courseids) {
    query = Qs.stringify({
      options: {
        ids: courseids
      }
    });
  } else {
    query = "";
  }
  moodle_rest_call(token, "core_course_get_courses", query, callback);
};

exports.getSingleCourseByID = function(token, courseid, callback) {
  exports.getCoursesByID(token, [courseid], function(err, courses) {
    if (err) {
      callback(err);
    } else {
      callback(null, courses[0]);
    }
  });
};

/*
 * Convenience function
 */
exports.getAllCourses = function(token, callback) {
  this.getCoursesByID(token, null, callback);
}

/*
 * Get all users that are enrolled in a course.
 */
exports.getEnrolledUsers = function(token, courseid, callback) {
  var query = Qs.stringify({
    courseid: courseid
  });
  moodle_rest_call(token, "core_enrol_get_enrolled_users", query, callback);
};

/*
 * Get detailed information about the structure of a course. This  includes
 * downloadable links and timestamps for all files.
 */
exports.getCourseContents = function(token, courseid, callback) {
  var query = Qs.stringify({
    courseid: courseid
  });
  moodle_rest_call(token, "core_course_get_contents", query, callback);
};

/*
 * Get all courses of a given user.
 */
exports.getAllCoursesOfUser = function(token, userid, callback) {
  var query = Qs.stringify({
    userid: userid
  });
  moodle_rest_call(token, "core_enrol_get_users_courses", query, callback);
};

/*
 * Rather useless: It only allows uploads to a user's private files.
 */
exports.uploadFile = function(token, local_file, remote_path, callback) {
  var formData = {
    filepath: remote_path,
    token: token,
    file_box: fs.createReadStream(local_file)
  };
  request.post({
    url: "http://" + moodle_host + moodle_upload,
    headers: {
      "User-Agent": "nodejs"
    },
    formData: formData
  }, function(err, res, body) {
    if (err) {
      callback(err);
    } else {
      callback(null, body);
    }
  });
};

// create token for user:
// /login/token.php?username=&password=&service=shortname

/*
 * Returns an array containing the course IDs of all courses the user is
 * enrolled in.
 */
exports.getUserCourseIDs = function(token, userid, callback) {
  exports.getAllCoursesOfUser(token, userid, function(err, courses) {
    if (err) {
      callback(err);
    } else {
      callback(null, courses.map(function extractCourseID(course) {
        return course.id;
      }));
    }
  })
};

/*
 * Returns {id, name, timemodified} for all courses specified in courseids
 * array.
 */
exports.getCourses = function(token, courseids, callback) {
  exports.getCoursesByID(token, courseids, function(err, courses) {
    if (err) {
      callback(err);
    } else {
      callback(null, courses.map(function extractCourseInfo(course) {
        return {
          id: course.id,
          name: course.fullname,
          timemodified: course.timemodified
        };
      }));
    }
  });
}

/*
 * Like getCourses(), but returns information about all courses in the moodle.
 */
exports.getAllCourses = function(token, callback) {
  exports.getCourses(token, null, callback);
};

/*
 * Returns an array containing information about all files in a course.
 * Format: {filename, filepath, timemodified, fileurl}
 */
exports.getCourseFiles = function(token, courseid, callback) {
  exports.getCourseContents(token, courseid, function(err, sections) {
    if (err) {
      callback(err);
    } else {
      // remove all modules whose modplural value is not in implemented_modules
      sections.forEach(function(section) {
        section.modules = section.modules.filter(function keepFileModules(
          module) {
          return implemented_modules.indexOf(module.modplural) >= 0;
        });
      });

      // remove empty sections
      sections = sections.filter(function removeEmptyAndHiddenSections(section) {
        return section.modules.length != 0 && section.visible === 1;
      });

      // flatten filtered sections
      var files = flattenSections(token, sections);

      callback(null, files);
    }
  })
};

/*
 * Internal: Flatten files in section -> module -> content hierarchy to array of
 * {filename, filepath, timemodified, fileurl} objects.
 * Also filters hidden files.
 */
function flattenSections(token, sections) {
  var files = [];
  sections.forEach(function(section) {
    var filepath_prefix = section.name;
    section.modules.forEach(function(module) {
      if (module.visible === 1) {
        if (module.modplural === "Folders") {
          filepath_prefix += "/" + module.name;
        }
        module.contents.forEach(function(content) {
          assert.deepEqual(content.type, "file");
          files.push({
            filename: content.filename,
            filepath: filepath_prefix + content.filepath,
            timemodified: content.timemodified,
            fileurl: content.fileurl + "&token=" + token
          });
        });
      }
    });
  });
  return files;
};

exports.getUserCourseIDsByEmail = function(token, email, callback) {
  exports.getSingleUserBy(token, "email", email, function(err, user) {
    if (err) {
      callback(err);
    } else {
      exports.getUserCourseIDs(token, user.id, callback);
    }
  })
};
